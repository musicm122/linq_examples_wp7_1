﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using LinqExamples_Library;
using System.Collections.ObjectModel;
using Vici.CoolStorage;
using LinqExamples_Library.Message;
using LinqExamples_Library.MVVM;

namespace Linq_Example_WP71_Main
{
    public class SampleListViewModel : Linq_Example_WP71_Main.MVVM.ViewModelBase
    {
        public SampleListViewModel():base() 
        { }
        
        private SampleListViewModel _newPageViewModel;
        public SampleListViewModel NewPageViewModel 
        {
            get { return _newPageViewModel; }
            set
            {
                if (value != _newPageViewModel)
                {
                    _newPageViewModel = value;
                    NotifyPropertyChanged("NewPageModel");
                }
            }
        } 

        public ObservableCollection<Sample> Samples {get;set;}
        private SampleType _SelectedType;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public SampleType SelectedType
        {
            get
            {
                return _SelectedType;
            }
            set
            {
                if (value != _SelectedType)
                {
                    _SelectedType = value;
                    
                    NotifyPropertyChanged("SelectedType");
                }
            }
        }

        /// <summary>
        /// Creates and adds a few SampleListViewModel objects into the Items collection.
        /// </summary>
        public void LoadData()
        {
            this.Samples = null;
            this.IsDataLoaded = false;

            var sampleService = new LinqExamples_Library.DataServices.SampleService();
            this.Samples = new ObservableCollection<Sample>();

            var queryResults = sampleService.samples.Where("SampleTypeId=@SampleTypeId", "@SampleTypeId", this.SelectedType.PK);
            foreach (Sample item in queryResults)
            {
                this.Samples.Add(item);
            }
            this.IsDataLoaded = true;
        }
        
        public void SetSampleTypeByTypeId(string type)
        {
            var sampleTypeService = new LinqExamples_Library.DataServices.SampleTypeService();
            this.SelectedType = sampleTypeService.SampleTypes.Where("Type=@Type", "@Type", type).FirstItem;
            LoadData();
        }
    }
}
