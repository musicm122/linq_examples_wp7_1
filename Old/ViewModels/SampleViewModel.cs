﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using LinqExamples_Library;
using System.Collections.Generic;

namespace Linq_Example_WP71_Main
{
    public class SampleViewModel: Linq_Example_WP71_Main.MVVM.ViewModelBase
    {
        public SampleViewModel() : base() 
        { }
        private Sample _SelectedSample = new Sample();
        private string _lineOne;
        private string _header;
        private string _pk;

        private SampleViewModel _newPageViewModel;
        public SampleViewModel NewPageViewModel
        {
            get { return _newPageViewModel; }
            set
            {
                if (value != _newPageViewModel)
                {
                    _newPageViewModel = value;
                    NotifyPropertyChanged("NewPageModel");
                }
            }
        } 

        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string Header
        {
            get
            {
                return _SelectedSample.Header;
            }
            set
            {
                if (value != _SelectedSample.Header)
                {
                    _lineOne = value;
                    NotifyPropertyChanged("Header");
                }
            }
        }

        //private string _lineTwo;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string Body
        {
            get
            {
                return _SelectedSample.Body;
            }
            set
            {
                if (value != _SelectedSample.Body)
                {
                    _SelectedSample.Body = value;
                    NotifyPropertyChanged("Body");
                }
            }
        }

        //private string _lineThree;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string Description
        {
            get
            {
                return _SelectedSample.Description;
            }
            set
            {
                if (value != _SelectedSample.Description)
                {
                    _SelectedSample.Description = value;
                    NotifyPropertyChanged("Description");
                }
            }
        }

        public string Result
        {
            get
            {
                return _SelectedSample.Result;
            }
            set
            {
                if (value != _SelectedSample.Result)
                {
                    _SelectedSample.Result = value;
                    NotifyPropertyChanged("Result");
                }
            }
        }
        
        public Sample SelectedSample
        {
            get
            {
                return _SelectedSample;
            }
            set
            {
                if (value != _SelectedSample)
                {
                    _SelectedSample= value;
                    NotifyPropertyChanged("SelectedSample");
                }
            }
        }
        
        public void SetSampleByHeader(string header)
        {
            this._header = header;
            LoadData();
        }

        public void SetSampleByPK(string pk)
        {
            this._pk = pk;
            LoadData();
        }


        public void LoadData() 
        {
            this.IsDataLoaded = false;
            var sampleService = new LinqExamples_Library.DataServices.SampleService();
            this.SelectedSample = sampleService.samples.Where("Header=@Header", "@Header", _header).FirstItem;
            if (this.SelectedSample != null) { IsDataLoaded = true; }
            //this._SelectedSample = sampleService.samples.Where("PK=@PK", "@PK", pk).FirstItem;
            
        }
    }
}
