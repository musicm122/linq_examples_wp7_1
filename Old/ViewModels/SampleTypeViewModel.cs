﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using LinqExamples_Library;
using System.Collections.ObjectModel;
using Vici.CoolStorage;
using LinqExamples_Library.Message;
using LinqExamples_Library.MVVM;

namespace Linq_Example_WP71_Main
{
    public class SampleTypeViewModel : Linq_Example_WP71_Main.MVVM.ViewModelBase
    {
        public SampleTypeViewModel()
            : base() 
        { }
        
        private SampleTypeViewModel  _newPageViewModel;
        public SampleTypeViewModel  NewPageViewModel 
        {
            get { return _newPageViewModel; }
            set
            {
                if (value != _newPageViewModel)
                {
                    _newPageViewModel = value;
                    NotifyPropertyChanged("NewPageModel");
                }
            }
        } 

        public ObservableCollection<SampleType> SampleTypes {get;set;}
        private Category _SelectedCategory;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public Category SelectedCategory
        {
            get
            {
                return _SelectedCategory;
            }
            set
            {
                if (value != _SelectedCategory)
                {
                    _SelectedCategory = value;
                    
                    NotifyPropertyChanged("SelectedCategory");
                }
            }
        }

        /// <summary>
        /// Creates and adds a few SampleListViewModel objects into the Items collection.
        /// </summary>
        public void LoadData()
        {
            this.SampleTypes = null;
            this.IsDataLoaded = false;
            var catService = new LinqExamples_Library.DataServices.CategoryService();
            Category currentCategory = catService.categories.Where("Name=@Name", "@Name", this.SelectedCategory.Name).UniqueItem;
            this.SampleTypes = new ObservableCollection<SampleType>();
            var sampleService = new LinqExamples_Library.DataServices.SampleTypeService();
            foreach (var item in sampleService.SampleTypes.Where("CategoryId=@CategoryId", "@CategoryId", currentCategory.PK.ToString()))
            {
                this.SampleTypes.Add(item);
            }
            this.IsDataLoaded = true;
        }
        
        public void SetCategoryByPK(string pk)
        {
            var catService = new LinqExamples_Library.DataServices.CategoryService();
            this.SelectedCategory = catService.categories.Where("PK=@PK", "@PK", pk).FirstItem;
        }

        public void SetCategoryByName(string name)
        {
            var catService = new LinqExamples_Library.DataServices.CategoryService();
            this.SelectedCategory = catService.categories.Where("Name=@Name", "@Name", name).FirstItem;
            this.LoadData();
        }
    }
}
