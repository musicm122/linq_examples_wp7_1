﻿using System.ComponentModel;
using TinyMessenger;


namespace Linq_Example_WP71_Main.MVVM
{    
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        //private readonly TinyMessengerHub _messenger;

        public bool IsDataLoaded
        {
            get;
            set;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}