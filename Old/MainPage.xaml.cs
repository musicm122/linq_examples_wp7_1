﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Linq_Example_WP71_Main.View;
using LinqExamples_Library;
using LinqExamples_Library.Message;

namespace Linq_Example_WP71_Main
{
	public partial class MainPage : PhoneApplicationPage
	{

		// Constructor
		public MainPage()
		{
			InitializeComponent();

			// Set the data context of the listbox control to the sample data
			DataContext = App.CategoryViewModel;
			this.Loaded += new RoutedEventHandler(MainPage_Loaded);
			
		}
		// Load data for the ViewModel Items
		private void MainPage_Loaded(object sender, RoutedEventArgs e)
		{
            if (!App.CategoryViewModel.IsDataLoaded)
			{
                App.CategoryViewModel.LoadData();
			}
		}

		protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
		{
			base.OnNavigatedFrom(e);
		}

		
        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			selectedTextBlock = (TextBlock)sender;
			this.NavigationService.Navigate(new Uri(String.Format("/View/SampleTypeView.xaml?parameter={0}",selectedTextBlock.Text), UriKind.Relative));
		}
		TextBlock selectedTextBlock { get; set; }
	}
}
