﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using LinqExamples_Library;
using LinqExamples_Library.DataServices;

namespace Linq_Example_WP71_Main.View
{
	public partial class SampleTypeView : PhoneApplicationPage
	{
		public SampleTypeView()
		{
			InitializeComponent();

			DataContext = App.SampleTypeViewModel;
			this.Loaded += new RoutedEventHandler(SampleTypeView_Loaded);
		}

		private static SampleTypeViewModel _viewModel = null;
		/// <summary>
		/// A static ViewModel used by the views to bind against.
		/// </summary>
		/// <returns>The MainViewModel object.</returns>
		public static SampleTypeViewModel ViewModel
		{
			get
			{
				// Delay creation of the view model until necessary
				if (_viewModel == null)
					_viewModel = new SampleTypeViewModel();

				return _viewModel;
			}
		}

		// Load data for the ViewModel Items
		private void SampleTypeView_Loaded(object sender, RoutedEventArgs e)
		{
			if (!App.SampleTypeViewModel.IsDataLoaded)
			{
				App.SampleTypeViewModel.LoadData();
			}
		}
		
		/*
		public SampleTypeView (Category categorySelected)
		{
			InitializeComponent();
			//STypeViewModel = new SampleTypeView Model(categorySelected);
			//STypeViewModel.LoadData();
			//DataContext = this.STypeViewModel;
		}*/
		
		protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
		{
			base.OnNavigatedTo(e);
			
			App.SampleTypeViewModel.SetCategoryByName(NavigationContext.QueryString["parameter"]);
			this.DataContext = App.SampleTypeViewModel;
			this.SampleTypeList.ItemsSource = App.SampleTypeViewModel.SampleTypes;
			
			//this.STypeViewModel = new SampleListViewModel();
			//STypeViewModel.SetCategoryByName(N);
			/*
			if (!this.STypeViewModel.IsDataLoaded)
			{
				this.STypeViewModel.LoadData();
			}
			//this.SampleTypeList.ItemsSource=this.STypeViewModel.
			this.DataContext = this.STypeViewModel;
			 */
		}

		private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			selectedTextBlock = (TextBlock)sender;
			this.NavigationService.Navigate(new Uri(String.Format("/View/SampleListView.xaml?parameter={0}", selectedTextBlock.Text), UriKind.Relative));
		}
		TextBlock selectedTextBlock { get; set; }

	}
}