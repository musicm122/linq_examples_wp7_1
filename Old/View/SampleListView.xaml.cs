﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using LinqExamples_Library;
using LinqExamples_Library.DataServices;

namespace Linq_Example_WP71_Main.View
{
	public partial class SampleListView : PhoneApplicationPage
	{
		public SampleListView()
		{
			InitializeComponent();

			DataContext = App.SampleListViewModel;
			this.Loaded += new RoutedEventHandler(SampleListView_Loaded);
		}

		private static SampleListViewModel _viewModel = null;
		/// <summary>
		/// A static ViewModel used by the views to bind against.
		/// </summary>
		/// <returns>The MainViewModel object.</returns>
		public static SampleListViewModel ViewModel
		{
			get
			{
				// Delay creation of the view model until necessary
				if (_viewModel == null)
					_viewModel = new SampleListViewModel();

				return _viewModel;
			}
		}

		// Load data for the ViewModel Items
		private void SampleListView_Loaded(object sender, RoutedEventArgs e)
		{
			if (!App.SampleListViewModel.IsDataLoaded)
			{
				App.SampleListViewModel.LoadData();
			}
		}
		
		
		
		protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
		{
			base.OnNavigatedTo(e);

			App.SampleListViewModel.SetSampleTypeByTypeId(NavigationContext.QueryString["parameter"]);
			this.DataContext = App.SampleListViewModel;
			this.SampleList.ItemsSource = App.SampleListViewModel.Samples;
			
		}

		private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			selectedTextBlock = (TextBlock)sender;
			this.NavigationService.Navigate(new Uri(String.Format("/View/SampleDetailView.xaml?parameter={0}", selectedTextBlock.Text), UriKind.Relative));
		}
		TextBlock selectedTextBlock { get; set; }

	}
}