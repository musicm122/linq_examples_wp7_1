﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Codexcite.SyntaxHighlighting;   

namespace Linq_Example_WP71_Main.View
{
    public partial class SampleDetailView : PhoneApplicationPage
    {
        public SampleDetailView()
        {
            InitializeComponent();
            DataContext = App.SampleViewModel;
            this.Loaded += new RoutedEventHandler(SampleDetailView_Loaded);
        }

        private static SampleViewModel _viewModel = null;
        /// <summary>
        /// A static ViewModel used by the views to bind against.
        /// </summary>
        /// <returns>The MainViewModel object.</returns>
        public static SampleViewModel ViewModel
        {
            get
            {
                // Delay creation of the view model until necessary
                if (_viewModel == null)
                    _viewModel = new SampleViewModel();

                return _viewModel;
            }
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            
            App.SampleViewModel.SetSampleByHeader(NavigationContext.QueryString["parameter"]);
            this.DataContext = App.SampleViewModel;
            
            this.SampleDisplay.DataContext = App.SampleViewModel.SelectedSample;
            this.HeaderTxt.Text= App.SampleViewModel.Header;
            this.BodyTxt.IsReadOnly = true;
            this.BodyTxt.SourceCode = App.SampleViewModel.Body;
            this.DescriptionTxt.Text = App.SampleViewModel.Description;
            this.ResultTxt.Text = App.SampleViewModel.Result;
            
        }
        // Load data for the ViewModel Items
        private void SampleDetailView_Loaded(object sender, RoutedEventArgs e)
        {
            if (!App.SampleViewModel.IsDataLoaded)
            {
                App.SampleViewModel.LoadData();
            }
        }

        private void BodyTxt_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.Focus();
        }

        private void BodyTxt_DoubleTap(object sender, GestureEventArgs e)
        {
            this.Focus();
        }
    }
}