﻿using Vici.CoolStorage;

namespace LinqExamples_Library.Models
{
    [MapTo("Sample")]
    public class Sample : CSObject<Sample, int>
    {
        public Sample() { }

        public string Body
        {
            get { return (string)GetField("Body"); }
            set { SetField("Body", value); }
        }

        public string Description
        {
            get { return (string)GetField("Description"); }
            set { SetField("Description", value); }
        }

        public string Header
        {
            get { return (string)GetField("Header"); }
            set { SetField("Header", value); }
        }

        public int PK { get { return (int)GetField("PK"); } }

        public string Result
        {
            get { return (string)GetField("Result"); }
            set { SetField("Result", value); }
        }

        public string SampleTypeId
        {
            get { return (string)GetField("SampleTypeId"); }
            set { SetField("SampleTypeId", value); }
        }
    }

    /*
    enum Grouping
    {
        RestrictionOperators,
        ProjectionOperators,
        GroupingOperators,
        AggregateOperators,
        SetOperators,
        ConversionOperators,
        MiscellaneousOperators,
        PartitioningOperators,
        ElementOperators,
        CustomSequenceOperators,
        QueryExecution,
        GenerationOperators,
        OrderingOperators,
        Quantifiers,
        UtilityRoutine,
        JoinOperators
    }

    enum Category
    {
        Where,
        Select,
        SelectMany,
        GroupBy,
        Count,
        Sum,
        Min,
        Max,
        Average,
        Aggregate,
        Distinct,
        Union,
        Intersect,
        Except,
        Concat
    }*/
}