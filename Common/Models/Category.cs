﻿using Vici.CoolStorage;

namespace LinqExamples_Library.Models
{
    [MapTo("Category")]
    public class Category : CSObject<Category, int>
    {
        public Category() { }

        public string Name
        {
            get { return (string)GetField("Name"); }
            set { SetField("Name", value); }
        }

        public int PK { get { return (int)GetField("PK"); } }
    }
}