﻿using Vici.CoolStorage;

namespace LinqExamples_Library.Models
{
    [MapTo("SampleType")]
    public class SampleType : CSObject<SampleType, int>
    {
        public SampleType() { }

        public string CategoryId
        {
            get { return (string)GetField("CategoryId"); }
            set { SetField("CategoryId", value); }
        }

        public int PK { get { return (int)GetField("PK"); } }

        public string Type
        {
            get { return (string)GetField("Type"); }
            set { SetField("Type", value); }
        }
    }
}