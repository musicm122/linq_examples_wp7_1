﻿using System;
using LinqExamples_Library.Message;
using Microsoft.Phone.Controls;
using TinyIoC;
using TinyMessenger;
using System.Windows.Navigation;

using Linq_Example_WP71_Main.ViewModels;
using System.Windows.Controls;

namespace Linq_Example_WP71_Main
{
    public partial class MainPage : PhoneApplicationPage
    {
        public MainPage()
        {
            InitializeComponent();
            var messageHub = TinyIoCContainer.Current.Resolve<ITinyMessengerHub>();
            messageHub.Unsubscribe<NavigationMessage>(new TinyMessageSubscriptionToken(messageHub, typeof(NavigationMessage)));
            messageHub.Subscribe<NavigationMessage>(Navigate);
        }

        public void Navigate(NavigationMessage message)
        {
            var navLocation = new Uri(String.Format("{0}", message.Sender), UriKind.Relative);
            this.NavigationService.Navigate(navLocation);
        }

        private void ApplicationBarMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/YourLastAboutDialog;component/AboutPage.xaml", UriKind.Relative));
        }
    }
}