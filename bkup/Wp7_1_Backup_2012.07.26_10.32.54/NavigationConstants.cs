﻿namespace Linq_Example_WP71_Main
{
    public class NavigationConstants
    {
        public const string CategoryLocation = "/View/MainPage.xaml";
        public const string SampleDetailLocation = "/View/SampleDetailView.xaml";
        public const string SampleDetailLocationParam = "SampleId";
        public const string SampleTypeLocation = "/View/SampleTypeView.xaml";
        public const string SampleTypeLocationParam = "CategoryId";
    }
}