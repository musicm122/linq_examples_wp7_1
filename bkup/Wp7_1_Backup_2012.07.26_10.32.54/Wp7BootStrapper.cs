﻿using System;
using Linq_Example_WP71_Main.ViewModels;
using LinqExamples_Library;
using LinqExamples_Library.Interfaces;
using LinqExamples_Library.Message;
using Microsoft.Phone.Controls;
using TinyIoC;
using TinyMessenger;
using Telerik.Windows.Controls;
using System.Diagnostics;

namespace Linq_Example_WP71_Main
{
    public class Wp7BootStrapper : Bootstrapper
    {
        public Wp7BootStrapper(PhoneApplicationFrame RootFrame)
            : base()
        {
            //base.Init();
            _RootFrame = RootFrame;
        }

        private PhoneApplicationFrame _RootFrame { get; set; }

        public override void InitDataServiceHelpers()
        {
            TinyIoCContainer.Current.Resolve<MainViewModel>().LoadData();
        }

        //public override void InitNavigationMessaging()
        //{
        //    //Implement App wide Page navigation via messaging
        //    base.InitNavigationMessaging();
        //    var messageHub = TinyIoCContainer.Current.Resolve<ITinyMessengerHub>();
        //    messageHub.Subscribe<NavigationMessage>((m) =>
        //    {
        //        var nav = new Uri(String.Format("{0}?parameter={1}", m.Sender, m.Content), UriKind.Relative);
        //        _RootFrame.Navigate(nav);
        //    });
        //}

        public override void InitErrorHandling()
        {
            base.InitErrorHandling();
            var diag = TinyIoCContainer.Current.Resolve<RadDiagnostics>();
            diag.ApplicationName = "Linq Examples";
            diag.EmailTo = "musicm122@gmail.com";
            diag.ExceptionOccurred += new EventHandler<ExceptionOccurredEventArgs>(diag_ExceptionOccurred);
            diag.Init();
        }

        void diag_ExceptionOccurred(object sender, ExceptionOccurredEventArgs e)
        {
            var d = (RadDiagnostics)sender;
            d.MessageBoxInfo.Content += e.ToString();
            System.Diagnostics.Debugger.Launch();
            System.Diagnostics.Debugger.Break();
            Debug.WriteLine(e);
        }
       

        public override void RegisterMembers()
        {
            TinyIoCContainer.Current.Register<MainViewModel>().AsSingleton();
            TinyIoCContainer.Current.Register<IDataInitializer, WP7DataInitializer>();
            TinyIoCContainer.Current.Register<RadDiagnostics>().AsSingleton();
        }
    }
}