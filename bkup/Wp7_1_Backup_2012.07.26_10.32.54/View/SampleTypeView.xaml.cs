﻿using System;
using LinqExamples_Library.Message;
using Microsoft.Phone.Controls;
using TinyIoC;
using TinyMessenger;
using System.Windows.Navigation;

using System.Windows.Controls;
using Linq_Example_WP71_Main.ViewModels;

namespace Linq_Example_WP71_Main.View
{
    public partial class SampleTypeView : PhoneApplicationPage
    {
        public SampleTypeView()
        {
            InitializeComponent();
            var messageHub = TinyIoCContainer.Current.Resolve<ITinyMessengerHub>();
            messageHub.Unsubscribe<NavigationMessage>(new TinyMessageSubscriptionToken(messageHub, typeof(NavigationMessage)));
            messageHub.Subscribe<NavigationMessage>(Navigate);
        }

        public void Navigate(NavigationMessage message)
        {
            var navLocation = new Uri(String.Format("{0}", message.Sender), UriKind.Relative);
            this.NavigationService.Navigate(navLocation);
        }

    }
}