﻿using System;
using System.Windows;
using System.Windows.Controls;
using LinqExamples_Library;
using Microsoft.Phone.Controls;

//WORK FROM HERE
//WORK FROM HERE
//WORK FROM HERE
namespace Linq_Example_WP71_Main.View
{
    public partial class SampleListView : PhoneApplicationPage
    {
        public SampleListView()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(SampleListView_Loaded);
        }

        // Load data for the ViewModel Items
        private void SampleListView_Loaded(object sender, RoutedEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            this.DataContext = App.ViewModel;
            App.ViewModel.SampleTypeCollection.Clear();
            App.ViewModel.SetSampleTypeByTypeId(NavigationContext.QueryString["parameter"]);
            //this.SampleList.ItemsSource = App.ViewModel.SampleCollection;
        }

        private void SampleList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.TypeSamples.SelectedIndex != -1)
            {
                Sample item = (sender as ListBox).SelectedItem as Sample;
                this.NavigationService.Navigate(new Uri(String.Format("/View/SampleDetailView.xaml?parameter={0}", item.PK), UriKind.Relative));
            }
        }
    }
}