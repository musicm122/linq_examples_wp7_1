﻿using System.Collections.ObjectModel;
using LinqExamples_Library.Extensions;
using LinqExamples_Library.Models;
using Vici.CoolStorage;

namespace Linq_Example_WP71_Main.ViewModels
{
    public class SampleTypeDataAdapter : GalaSoft.MvvmLight.ViewModelBase
    {
        /// <summary>
        /// The <see cref="Type" /> property's name.
        /// </summary>
        public const string TypePropertyName = "Type";

        private SampleType _type = null;

        public SampleTypeDataAdapter()
        {
        }

        public SampleTypeDataAdapter(SampleType type)
        {
            type = this.Type;
        }

        public ObservableCollection<Sample> SampleCollection
        {
            get
            {
                return Sample.OrderedList("Header", new CSFilter("PK=@PK", "@PK", this.Type.PK)).ToObservableCollection<Sample>();
            }
        }

        /// <summary>
        /// Sets and gets the Type property.
        /// Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public SampleType Type
        {
            get
            {
                return _type;
            }

            set
            {
                if (_type == value)
                {
                    return;
                }

                _type = value;
                RaisePropertyChanged(TypePropertyName);
            }
        }
    }
}