﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using LinqExamples_Library.Models;

namespace Linq_Example_WP71_Main.ViewModels
{
    public class StoredState
    {
        public StoredState()
        {
            this.StateMembers = new Dictionary<string, int>();
        }

        public void Save(MainViewModel vm) 
        {
            this.Save(vm.SelectedCategory);
            this.Save(vm.SelectedSample);
            this.Save(vm.SelectedSampleType);
        }

        public void Save(Category selected)
        {
            if (selected != null)
            {
                StateMembers.Add("SelectedCategoryPK", selected.PK);
            }
        }

        public void Save(SampleType selected)
        {
            if (selected != null)
            {
                StateMembers.Add("SelectedSampleTypePK", selected.PK);
            }
        }

        public void Save(Sample selected)
        {
            if (selected != null)
            {
                StateMembers.Add("SelectedSamplePK", selected.PK);
            }
        }

        public void Load(ref MainViewModel vm)
        {
            if (StateMembers["SelectedCategoryPK"] != null)
            {
                vm.SelectedCategory = Category.All().Where("PK=@Id", "@Id", StateMembers["SelectedCategoryPK"]).FirstItem;
            }

            if (StateMembers["SelectedSampleTypePK"] != null)
            {
                vm.SelectedSampleType = SampleType.All().Where("PK=@Id", "@Id", StateMembers["SelectedSampleTypePK"]).FirstItem;
            }

            if (StateMembers["SelectedSampleTypePK"] != null)
            {
                vm.SelectedSample = Sample.All().Where("PK=@Id", "@Id", StateMembers["SelectedSamplePK"]).FirstItem;
            }
        }
        
        public Dictionary<string, int> StateMembers { get; set; }


    }
}
