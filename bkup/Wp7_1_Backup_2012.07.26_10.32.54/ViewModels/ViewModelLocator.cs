﻿using Linq_Example_WP71_Main.ViewModels;
using TinyIoC;

namespace Linq_Example_WP71_Main
{
    public class ViewModelLocator
    {
        public ViewModelLocator() { }

        public MainViewModel ViewModel
        {
            get
            {
                return TinyIoCContainer.Current.Resolve<MainViewModel>();
            }
        }
    }
}