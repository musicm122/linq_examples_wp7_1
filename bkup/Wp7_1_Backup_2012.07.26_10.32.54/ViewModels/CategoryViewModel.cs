﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using LinqExamples_Library;
using LinqExamples_Library.DataServices;
using LinqExamples_Library.MVVM;

namespace Linq_Example_WP71_Main
{
    public class CategoryViewModel : ViewModelBase
    {
        public CategoryViewModel()
        {
            this.Categories = new ObservableCollection<Category>();
            this.Items = new ObservableCollection<SampleListViewModel>();
            this.LoadData();
            this.SelectedCategory = (this.Categories[0]);
            //SelectCommand = new RelayCommand(SelectCategory);
        }

        //private readonly TinyMessengerHub _messenger;
        /// <summary>
        /// A collection for SampleListViewModel objects.
        /// </summary>
        public ICommand SelectCommand { get; private set; }

        public ObservableCollection<Category> Categories { get; private set; }

        public ObservableCollection<SampleListViewModel> Items { get; private set; }

        private Category _selectedCategory;

        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding
        /// </summary>
        /// <returns></returns>
        public Category SelectedCategory
        {
            get
            {
                return _selectedCategory;
            }
            set
            {
                if (value != _selectedCategory)
                {
                    _selectedCategory = value;
                    RaisePropertyChanged("SelectedCategory");
                }
            }
        }

        public bool IsDataLoaded
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates and adds a few SampleListViewModel objects into the Items collection.
        /// </summary>
        public void LoadData()
        {
            this.Categories = new ObservableCollection<Category>();
            var CategoryService = new LinqExamples_Library.DataServices.CategoryService();

            foreach (var item in CategoryService.categories)
            {
                this.Categories.Add(item);
            }

            // Sample data; replace with real data
            this.IsDataLoaded = true;
        }
    }
}