﻿using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Windows.Resources;
using LinqExamples_Library.Interfaces;
using Vici.CoolStorage;

namespace Linq_Example_WP71_Main
{
    public class WP7DataInitializer : IDataInitializer
    {
        public override void InitializeDatabase(string fn = "LinqExamples.sqlite")
        {
            //string fn = "LinqExamples.sqlite";

            StreamResourceInfo sr = Application.GetResourceStream(new Uri(fn, UriKind.Relative));

            IsolatedStorageFile iStorage = IsolatedStorageFile.GetUserStoreForApplication();

            if (!iStorage.FileExists(fn))
            {
                using (var outputStream = iStorage.OpenFile(fn, FileMode.CreateNew))
                {
                    byte[] buffer = new byte[10000];

                    for (; ; )
                    {
                        int read = sr.Stream.Read(buffer, 0, buffer.Length);

                        if (read <= 0)
                            break;

                        outputStream.Write(buffer, 0, read);
                    }
                }
            }
            sr.Stream.Close();
            CSConfig.SetDB(fn);
        }
    }
}