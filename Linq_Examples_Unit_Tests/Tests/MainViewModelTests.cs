﻿using Linq_Example_WP71_Main.ViewModels;
using LinqExamples_Library.Models;
using Microsoft.Silverlight.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Vici.CoolStorage;
using TinyIoC;

namespace Linq_Examples_Unit_Tests.Tests
{
    [TestClass]
    public class MainViewModelTests : SilverlightTest
    {
        [TestMethod]
        public void Get_SampleCollection_On_Category_Set()
        {
            var vm = InitMainViewModel();
            var selectedCat = Category.Read(1);

            //Set Selected Category
            vm.NavigateToSampleTypeCommand.Execute(selectedCat);
            Assert.IsNotNull(vm.SampleTypeDataAdapterCollection);
            Assert.IsTrue(vm.SampleTypeDataAdapterCollection.Count > 0);
            int numberOfSamples = vm.SampleTypeDataAdapterCollection[0].SampleCollection.Count;

            Assert.IsTrue(numberOfSamples > 1);
        }

        public MainViewModel InitMainViewModel()
        {
            return new MainViewModel();
        }

        [TestInitialize]
        public void InitTests()
        {
            if (!CSConfig.HasDB())
            {
                var init = new Linq_Example_WP71_Main.WP7DataInitializer();
                init.InitializeDatabase();
            }
        }

        [TestMethod]
        public void Instiantiate_MainViewModel()
        {
            MainViewModel vm = new MainViewModel();
            Assert.IsNotNull(vm);
            Assert.IsTrue(vm.CategoryCollection.Count > 0);
            Assert.IsInstanceOfType(vm, typeof(MainViewModel));
        }

        [TestMethod]
        public void Instiantiate_MainViewModel_From_Container()
        {
            var vm = TinyIoCContainer.Current.Resolve<MainViewModel>();
            Assert.IsNotNull(vm);
            Assert.IsTrue(vm.CategoryCollection.Count > 0);
            Assert.IsInstanceOfType(vm, typeof(MainViewModel));
        }

        [TestMethod]
        public void Set_SelectedCategoryType_On_Navigation_To_Sample_Type()
        {
            var vm = InitMainViewModel();
            foreach (Category cat in Category.All())
            {
                vm.NavigateToSampleTypeCommand.Execute(cat);
                Assert.AreEqual(cat, vm.SelectedCategory);
            }
        }

        [TestMethod]
        public void Set_SelectedSample_On_Navigation()
        {
            var vm = InitMainViewModel();
            foreach (Sample sample in Sample.All())
            {
                vm.NavigateToSampleCommand.Execute(sample.Header);
                Assert.AreEqual(sample, vm.SelectedSample, "Sample header=" + sample.Header + " and selected sample header=" + vm.SelectedSample.Header);
            }
        }
    }
}