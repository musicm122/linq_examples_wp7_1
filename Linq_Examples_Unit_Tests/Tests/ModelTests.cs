﻿using LinqExamples_Library.Models;
using Microsoft.Silverlight.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vici.CoolStorage;

namespace Linq_Examples_Unit_Tests.Tests
{
    [TestClass]
    public class ModelTests : SilverlightTest
    {
        [TestMethod]
        public void Has_Loaded_DB()
        {
            Assert.IsTrue(CSConfig.HasDB());
        }

        [TestInitialize]
        public void InitTests()
        {
            if (!CSConfig.HasDB())
            {
                var init = new Linq_Example_WP71_Main.WP7DataInitializer();
                init.InitializeDatabase();
            }
        }

        [TestMethod]
        public void Instantiate_Category()
        {
            Category cat = Category.Read(1);
            Assert.IsTrue(Category.Count() > 0);
            Assert.IsNotNull(cat);
        }

        [TestMethod]
        public void Instantiate_CategoryList()
        {
            CSList<Category> cats = Category.All();
            Assert.IsNotNull(cats);
            Assert.IsTrue(cats.Count > 0);
        }

        [TestMethod]
        public void Instantiate_Sample()
        {
            Sample sample = Sample.Read(1);
            Assert.IsTrue(Sample.Count() > 0);
            Assert.IsNotNull(sample);
        }

        [TestMethod]
        public void Instantiate_SampleType()
        {
            SampleType sampletype = SampleType.Read(1);
            Assert.IsTrue(SampleType.Count() > 0);
            Assert.IsNotNull(sampletype);
        }

        [TestMethod]
        public void Sample_List_Max_Size()
        {
            int ExpectedCount = 100;
            int ActualCount = Sample.All().Count;
            Assert.AreEqual(ExpectedCount, ActualCount);
        }
    }
}