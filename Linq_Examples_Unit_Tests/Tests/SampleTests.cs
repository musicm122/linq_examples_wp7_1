﻿using LinqExamples_Library.Models;
using Microsoft.Silverlight.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vici.CoolStorage;

namespace Unit_Tests_Linq_Examples.Tests
{
    [TestClass]
    public class SampleTests : SilverlightTest
    {
        [TestInitialize]
        public void InitTests()
        {
            if (!CSConfig.HasDB())
            {
                var init = new Linq_Example_WP71_Main.WP7DataInitializer();
                init.InitializeDatabase();
            }
        }

        [TestMethod]
        public void Test_Sample_List_Max_Size()
        {
            int ExpectedCount = 100;
            int ActualCount = Sample.All().Count;
            Assert.AreEqual(ExpectedCount, ActualCount);
        }
    }
}