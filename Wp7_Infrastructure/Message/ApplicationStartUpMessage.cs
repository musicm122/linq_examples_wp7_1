﻿using TinyMessenger;

namespace Wp7_Infrastructure.Message
{
    public class ApplicationStartUpMessage : ITinyMessage
    {
        public object Sender
        {
            get;
            private set;
        }
    }
}