﻿using TinyMessenger;

namespace Wp7_Infrastructure.Message
{
    public class NavigationMessage : GenericTinyMessage<string>
    {
        public NavigationMessage(object sender, string args)
            : base(sender, args)
        {
        }
    }
}