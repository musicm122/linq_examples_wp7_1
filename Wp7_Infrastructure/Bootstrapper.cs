﻿using TinyIoC;
using TinyMessenger;
using Wp7_Infrastructure.Interfaces;
using Wp7_Infrastructure.Message;

namespace Wp7_Infrastructure
{
    public class Bootstrapper
    {
        public Bootstrapper()
        {
            var messageHub = TinyIoCContainer.Current.Resolve<ITinyMessengerHub>();

            messageHub.Subscribe<ApplicationStartUpMessage>((x) =>
            {
                Init();
            });
            messageHub.Publish<ApplicationStartUpMessage>(new ApplicationStartUpMessage());
        }

        public void Init()
        {
            RegisterMembers();
            InitErrorHandling();
            InitDB();
            InitDataServiceHelpers();
            InitNavigationMessaging();
            InitComplete();
        }

        public virtual void InitDataServiceHelpers()
        {
        }

        public virtual void InitDB()
        {
            TinyIoCContainer.Current.Resolve<IDataInitializer>().InitializeDatabase();
        }

        public virtual void InitErrorHandling()
        {
        }

        public virtual void InitNavigationMessaging()
        {
        }

        public virtual void RegisterMembers()
        {
            TinyIoCContainer.Current.Register<NavigationMessage>();
        }

        public virtual void InitComplete() 
        {
            
        }
    }
}