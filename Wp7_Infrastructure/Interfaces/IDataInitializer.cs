﻿namespace Wp7_Infrastructure.Interfaces
{
    public abstract class IDataInitializer
    {
        public abstract void InitializeDatabase(string fn = "LinqExamples.sqlite");
    }
}