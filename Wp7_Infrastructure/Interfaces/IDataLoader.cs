﻿using System.Collections.Generic;
using Vici.CoolStorage;

namespace Wp7_Infrastructure.Interfaces
{
    public abstract class IDataLoader<T> where T : CSObject
    {
        public abstract List<T> DBLoad();
    }
}