﻿using System;
using Microsoft.Phone.Controls;
using TinyIoC;
using TinyMessenger;
using Wp7_Infrastructure.Message;

namespace Linq_Example_WP71_Main.View
{
    public partial class SampleTypeView : PhoneApplicationPage
    {
        public SampleTypeView()
        {
            InitializeComponent();
            var messageHub = TinyIoCContainer.Current.Resolve<ITinyMessengerHub>();
            using (var subToken = new TinyMessageSubscriptionToken(messageHub, typeof(NavigationMessage))) 
            {
                messageHub.Unsubscribe<NavigationMessage>(subToken);
            }
            messageHub.Subscribe<NavigationMessage>(Navigate);
        }

        public void Navigate(NavigationMessage message)
        {
            var navLocation = new Uri(String.Format("{0}", message.Sender), UriKind.Relative);
            this.NavigationService.Navigate(navLocation);
        }
    }
}