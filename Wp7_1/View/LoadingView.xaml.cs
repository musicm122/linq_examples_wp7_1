﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using TinyIoC;
using Linq_Example_WP71_Main.ViewModels;
using TinyMessenger;
using Wp7_Infrastructure.Message;

namespace Linq_Example_WP71_Main.View
{
    public partial class LoadingView : PhoneApplicationPage
    {
        public LoadingView()
        {
          //ClearBackstack();
            InitializeComponent();
            var messageHub = TinyIoCContainer.Current.Resolve<ITinyMessengerHub>();
            using (var subToken = new TinyMessageSubscriptionToken(messageHub, typeof(NavigationMessage)))
            {
                messageHub.Unsubscribe<NavigationMessage>(subToken);
            }
            messageHub.Subscribe<NavigationMessage>(Navigate);
            

        }
        
        public void ClearBackstack()
        {
            while (this.NavigationService.BackStack.Any())
            {
                this.NavigationService.RemoveBackEntry();
            }
        }
        public void Navigate(NavigationMessage message)
        {
            var navLocation = new Uri(String.Format("{0}", message.Sender), UriKind.Relative);
            this.NavigationService.Navigate(navLocation);
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            Wp7BootStrapper bootstrapper = new Wp7BootStrapper();
            //this.NavigationService.RemoveBackEntry();
            
            TinyIoCContainer.Current.Resolve<MainViewModel>().ExecuteNavigation(NavigationConstants.CategoryLocation);
            
        }
    }
}