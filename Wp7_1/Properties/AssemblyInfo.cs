using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Linq Examples")]
[assembly: AssemblyDescription("Quick Ref App for Linq Examples")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hacker Ferret")]
[assembly: AssemblyProduct("Linq Examples")]
[assembly: AssemblyCopyright("Copyright �  2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("45b0a3eb-abf8-4ae6-8963-cbab5cc53fe5")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.2")]
[assembly: AssemblyFileVersion("1.0.0.2")]
[assembly: InternalsVisibleTo("Linq_Examples_Unit_Tests")]
[assembly: InternalsVisibleTo("Vici.Core.WP7")]
[assembly: InternalsVisibleTo("Vici.CoolStorage.WP7")]
[assembly: InternalsVisibleTo("Wp7_Infrastructure")]
[assembly: InternalsVisibleTo("LinqExamples_Library")]
[assembly: NeutralResourcesLanguageAttribute("en-US")]