﻿using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Windows.Navigation;
using Linq_Example_WP71_Main.ViewModels;
using Microsoft.Phone.Shell;
using Polenter.Serialization;
using Telerik.Windows.Controls;
using TinyIoC;
using System.Windows.Threading;
using System.ComponentModel;

namespace Linq_Example_WP71_Main
{
    //TODO: Fix application transitions Fadein fadeout-> slide
    public partial class App : Application
    {
        /// <summary>
        /// Constructor for the Application object.
        /// </summary>
        public App()
        {
            // Global handler for uncaught exceptions.
            UnhandledException += Application_UnhandledException;

            // Show graphics profiling information while debugging.
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // Display the current frame rate counters.
                Application.Current.Host.Settings.EnableFrameRateCounter = true;

                // Show the areas of the app that are being redrawn in each frame.
                //Application.Current.Host.Settings.EnableRedrawRegions = true;

                // Enable non-production analysis visualization mode,
                // which shows areas of a page that are being GPU accelerated with a colored overlay.
                //Application.Current.Host.Settings.EnableCacheVisualization = true;
            }

            // Standard Silverlight initialization
            InitializeComponent();

            // Phone-specific initialization
            InitializePhoneApplication();
            
            
        }

        #region Events

        // Code to execute when the application is activated (brought to foreground)
        // This code will not execute when the application is first launched
        private void Application_Activated(object sender, ActivatedEventArgs e)
        {
            //For use with RadRateApplicationReminder
            ApplicationUsageHelper.OnApplicationActivated();

            if (e.IsApplicationInstancePreserved)
            {
                Debug.WriteLine("Activated From Dormant State");
                PhoneApplicationService.Current.State.Clear();
            }
            else
            {
                Debug.WriteLine("Activated From Tombstoned State");
                LoadViewModelFromIsolatedStorage();
            }
        }

        // Code to execute when the application is closing (eg, user hit Back)
        // This code will not execute when the application is deactivated
        private void Application_Closing(object sender, ClosingEventArgs e)
        {
            // persist the data using isolated storage
            SaveViewModelToIsolatedStorage();
        }

        // Code to execute when the application is deactivated (sent to background)
        // This code will not execute when the application is closing
        private void Application_Deactivated(object sender, DeactivatedEventArgs e)
        {
            // Ensure that required application state is persisted here.

            //SaveViewModelToAppState();
            SaveViewModelToIsolatedStorage();
        }

        // Code to execute when the application is launching (eg, from Start)
        // This code will not execute when the application is reactivated
        private void Application_Launching(object sender, LaunchingEventArgs e)
        {
            //For use with RadRateApplicationReminder
            ApplicationUsageHelper.Init("2.2");

            //LoadViewModelFromIsolatedStorage();

            //// set the frame DataContext

            //RootFrame.DataContext = ViewModel;
        }

        // Code to execute on Unhandled Exceptions
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        // Code to execute if a navigation fails
        private void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // A navigation has failed; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        #endregion Events

        #region Properties

        /// <summary>
        /// Provides easy access to the root frame of the Phone Application.
        /// </summary>
        /// <returns>The root frame of the Phone Application.</returns>
        public RadPhoneApplicationFrame RootFrame { get; private set; }

        #endregion Properties

        #region Phone application initialization

        // Avoid double-initialization
        private bool phoneApplicationInitialized = false;

        // Do not add any additional code to this method
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            // Set the root visual to allow the application to render
            if (RootVisual != RootFrame)
                RootVisual = RootFrame;

            // Remove this handler since it is no longer needed
            RootFrame.Navigated -= CompleteInitializePhoneApplication;
        }

        // Do not add any additional code to this method
        private void InitializePhoneApplication()
        {
            if (phoneApplicationInitialized)
                return;
            // Create the frame but don't set it as RootVisual yet; this allows the splash
            // screen to remain active until the application is ready to render.
            RadTransition transition = new RadTransition();
            transition.BackwardInAnimation = this.Resources["fadeInAnimation"] as RadFadeAnimation;
            transition.BackwardOutAnimation = this.Resources["fadeOutAnimation"] as RadFadeAnimation;
            transition.ForwardInAnimation = this.Resources["fadeInAnimation"] as RadFadeAnimation;
            transition.ForwardOutAnimation = this.Resources["fadeOutAnimation"] as RadFadeAnimation;
            transition.PlayMode = TransitionPlayMode.Consecutively;
            RadPhoneApplicationFrame frame = new RadPhoneApplicationFrame();
            frame.Transition = transition;
            RootFrame = frame;
            RootFrame.Navigated += CompleteInitializePhoneApplication;
            // Handle navigation failures
            RootFrame.NavigationFailed += RootFrame_NavigationFailed;
            // Ensure we don't initialize again
            phoneApplicationInitialized = true;
        }

        #endregion Phone application initialization

        public static object Deserialize(Stream streamObject)
        {
            if (streamObject == null)
                return null;

            // true - binary serialization, default - xml ser.
            var serializer = new SharpSerializer(true);
            return serializer.Deserialize(streamObject);
        }

        public static void Serialize(Stream streamObject, object obj)
        {
            if (obj == null || streamObject == null)
                return;

            // true - binary serialization, default - xml ser.
            var serializer = new SharpSerializer(true);
            serializer.Serialize(obj, streamObject);
        }

        private void LoadViewModelFromIsolatedStorage()
        {
            using (IsolatedStorageFileStream stream = IsolatedStorageFile.GetUserStoreForApplication().OpenFile("data.txt", FileMode.Open))
            {
                var currVM = TinyIoCContainer.Current.Resolve<MainViewModel>();
                StoredState state = (StoredState)Deserialize(stream);
                state.Load(ref currVM);
                TinyIoCContainer.Current.Register<MainViewModel>(currVM);
            }
        }

        private void SaveViewModelToIsolatedStorage()
        {
            using (IsolatedStorageFileStream stream = IsolatedStorageFile.GetUserStoreForApplication().OpenFile("data.txt", FileMode.OpenOrCreate))
            {
                var currVM = TinyIoCContainer.Current.Resolve<MainViewModel>();
                StoredState state = new StoredState();
                state.Save(currVM);
                Serialize(stream, state);
            }
        }
    }
}