﻿using System;
using System.Diagnostics;
using Linq_Example_WP71_Main.ViewModels;
using Microsoft.Phone.Controls;
using Telerik.Windows.Controls;
using TinyIoC;
using Wp7_Infrastructure;
using Wp7_Infrastructure.Interfaces;
using TinyMessenger;
using Wp7_Infrastructure.Message;

namespace Linq_Example_WP71_Main
{
    public class Wp7BootStrapper : Bootstrapper
    {
        public Wp7BootStrapper()
            : base()
        {
            var messageHub = TinyIoCContainer.Current.Resolve<ITinyMessengerHub>();
        }

        public override void InitComplete()
        {
            base.InitComplete();
            
        }
        
        public override void InitErrorHandling()
        {
            base.InitErrorHandling();
            var diag = TinyIoCContainer.Current.Resolve<RadDiagnostics>();
            
            diag.ApplicationName = "Linq Examples";
            diag.EmailTo = "musicm122@gmail.com";
            diag.ExceptionOccurred += new EventHandler<ExceptionOccurredEventArgs>(diag_ExceptionOccurred);
            diag.Init();
        }

        public override void RegisterMembers()
        {
            TinyIoCContainer.Current.Register<MainViewModel>().AsSingleton();
            TinyIoCContainer.Current.Register<IDataInitializer, WP7DataInitializer>();
            TinyIoCContainer.Current.Register<RadDiagnostics>().AsSingleton();
        }

        private void diag_ExceptionOccurred(object sender, ExceptionOccurredEventArgs e)
        {
            var d = (RadDiagnostics)sender;
            d.MessageBoxInfo.Content += e.ToString();
            System.Diagnostics.Debugger.Launch();
            System.Diagnostics.Debugger.Break();
            Debug.WriteLine(e);
        }
    }
}