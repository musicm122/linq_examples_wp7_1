﻿using System;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using TinyIoC;
using TinyMessenger;
using Wp7_Infrastructure.Message;

namespace Linq_Example_WP71_Main
{
    public partial class MainPage : PhoneApplicationPage
    {
        public MainPage()
        {
            InitializeComponent();
            
            var messageHub = TinyIoCContainer.Current.Resolve<ITinyMessengerHub>();
            using (var subToken = new TinyMessageSubscriptionToken(messageHub, typeof(NavigationMessage))) 
            {
                messageHub.Unsubscribe<NavigationMessage>(subToken);
            }
            messageHub.Subscribe<NavigationMessage>(Navigate);
            
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            this.NavigationService.RemoveBackEntry();
        }

        public void Navigate(NavigationMessage message)
        {
            var navLocation = new Uri(String.Format("{0}", message.Sender), UriKind.Relative);
            this.NavigationService.Navigate(navLocation);
        }

        private void ApplicationBarMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/YourLastAboutDialog;component/AboutPage.xaml", UriKind.Relative));
        }
    }
}