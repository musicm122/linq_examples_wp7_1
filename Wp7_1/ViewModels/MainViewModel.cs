﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using GalaSoft.MvvmLight.Command;
using LinqExamples_Library.Extensions;
using LinqExamples_Library.Models;
using Polenter.Serialization;
using TinyIoC;
using TinyMessenger;

//using System.Diagnostics;
using Vici.CoolStorage;
using Wp7_Infrastructure.Message;

namespace Linq_Example_WP71_Main.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region Commands

        #region Navigation

        private RelayCommand<string> _NavigateToSample;
        private RelayCommand<object> _NavigateToSampleTypes;

        //
        /// <summary>
        /// Gets the NavigateToCategory.
        /// </summary>
        [ExcludeFromSerialization]
        public RelayCommand<string> NavigateToSampleCommand
        {
            get
            {
                ///
                return _NavigateToSample ?? (_NavigateToSample = new RelayCommand<string>((headerText) =>
                {
                    this.SelectedSample = Sample.ReadFirst(new CSFilter("Header=@Header", "@Header", headerText));
                    this.SelectedSampleType = SampleType.ReadFirst(new CSFilter("PK=@PK", "@PK", SelectedSample.SampleTypeId));
                    ExecuteNavigation(NavigationConstants.SampleDetailLocation, NavigationConstants.SampleDetailLocationParam + "=" + this.SelectedSample.PK.ToString());
                }));
            }
        }

        //
        /// <summary>
        /// Gets the NavigateToCategory.
        /// </summary>
        [ExcludeFromSerialization]
        public RelayCommand<object> NavigateToSampleTypeCommand
        {
            get
            {
                ///
                return _NavigateToSampleTypes ?? (_NavigateToSampleTypes = new RelayCommand<object>((_selectedCategory) =>
                {
                    this.SelectedCategory = (Category)_selectedCategory;
                    ExecuteNavigation(NavigationConstants.SampleTypeLocation, NavigationConstants.SampleTypeLocationParam + "=" + this.SelectedCategory.PK.ToString());
                }));
            }
        }

        public void ExecuteNavigation(string location, string args="")
        {
            var messageHub = TinyIoCContainer.Current.Resolve<ITinyMessengerHub>();
            messageHub.Publish<NavigationMessage>(new NavigationMessage(location, args));
        }

        #endregion Navigation

        #endregion Commands

        #region Fields

        #endregion Fields

        #region Constructor

        #endregion Constructor

        #region Dependency Properties

        #region Observable Collections

        #region CategoryCollection

        [ExcludeFromSerialization]
        public ObservableCollection<Category> CategoryCollection
        {
            get
            {
                return Category.List().ToObservableCollection<Category>();
            }
        }

        #endregion CategoryCollection

        #region SampleTypeCollection

        [ExcludeFromSerialization]
        public ObservableCollection<SampleType> SampleTypeCollection
        {
            get
            {
                return SampleType.List().FilteredBy("CategoryId=@Id", "@Id", SelectedCategory.PK).ToObservableCollection<SampleType>();
            }
        }

        #endregion SampleTypeCollection

        #region SampleTypeDataAdapterCollection

        [ExcludeFromSerialization]
        public ObservableCollection<SampleTypeDataAdapter> SampleTypeDataAdapterCollection
        {
            get
            {
                ObservableCollection<SampleTypeDataAdapter> retval = new ObservableCollection<SampleTypeDataAdapter>();
                var sampleTypes = SampleType.List().FilteredBy("CategoryId=@Id", "@Id", SelectedCategory.PK);
                sampleTypes.ForEach((st) =>
                {
                    retval.Add(new SampleTypeDataAdapter() { Type = st });
                });
                return retval;
            }
        }

        #endregion SampleTypeDataAdapterCollection

        #region SampleCollection

        [ExcludeFromSerialization]
        public ObservableCollection<Sample> SampleCollection
        {
            get
            {
                return Sample.List().FilteredBy("PK=@PK", "@PK", SelectedSampleType.PK).ToObservableCollection<Sample>();
            }
        }

        #endregion SampleCollection

        #endregion Observable Collections

        #region Selected Items

        #region SelectedCategory

        /// <summary>
        /// The <see cref="SelectedCategory" /> property's name.
        /// </summary>
        public const string SelectedCategoryPropertyName = "SelectedCategory";

        private Category _selectedCategory = null;

        /// <summary>
        /// Sets and gets the SelectedCategory property.
        /// Changes to that property's value raise the PropertyChanged event.
        /// </summary>

        public Category SelectedCategory
        {
            get
            {
                return _selectedCategory;
            }

            set
            {
                if (_selectedCategory == value)
                {
                    return;
                }

                _selectedCategory = value;

                NotifyPropertyChanged("SelectedCategory");
            }
        }

        #endregion SelectedCategory

        #region SelectedSample

        /// <summary>
        /// The <see cref="SelectedSample" /> property's name.
        /// </summary>
        public const string SelectedSamplePropertyName = "SelectedSample";

        private Sample _selectedSample = null;

        /// <summary>
        /// Sets and gets the SelectedSample property.
        /// Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public Sample SelectedSample
        {
            get
            {
                return _selectedSample;
            }

            set
            {
                if (_selectedSample == value)
                {
                    return;
                }

                _selectedSample = value;
                NotifyPropertyChanged("SelectedSamplePropertyName");
            }
        }

        #endregion SelectedSample

        #region SelectedSampleType

        /// <summary>
        /// The <see cref="SelectedSampleType" /> property's name.
        /// </summary>
        public const string SelectedSampleTypePropertyName = "SelectedSampleType";

        private SampleType _selectedSampleType = null;

        /// <summary>
        /// Sets and gets the SelectedSampleType property.
        /// Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public SampleType SelectedSampleType
        {
            get
            {
                return _selectedSampleType;
            }

            set
            {
                if (_selectedSampleType == value)
                {
                    return;
                }

                _selectedSampleType = value;
                NotifyPropertyChanged("SelectedSampleTypePropertyName");
            }
        }

        #endregion SelectedSampleType

        #endregion Selected Items

        #endregion Dependency Properties

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<SampleTypeDataAdapter> GetSampleTypeDataAdapters()
        {
            var adapters = new ObservableCollection<SampleTypeDataAdapter>();
            return adapters;
        }

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}